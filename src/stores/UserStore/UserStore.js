import { observable, action, flow, configure } from 'mobx';
import axios from 'axios';

configure({ enforceActions: 'strict' });

class UserStore {
    @observable signInMode = true;
    @observable user = null;
    @observable users = [];
    @observable error = null;
    @observable message = null;

    @action
    changeAuthMode = () => {
        this.signInMode = !this.signInMode;
        this.message = this.signInMode ? 'You are in Sign in mode' : 'You are in Sign Up mode';
    };
    @action signIn = (email, password) => this.sendSignInRequest(email, password);
    @action signUp = (email, password) => this.sendSignUpRequest(email, password);
    @action fetchLoggedUser = () => this.sendFetchLoggedUserRequest();
    @action fetchUsers = () => this.sendFetchUsersRequest();
    @action logout = () => this.resetUser();

    sendSignInRequest = flow(function*(email, password) {
        try {
            const response = yield axios.post('/signin', { email, password });
            const { token } = response.data;
            if (token) {
                this.user = email;
                this.saveUser(email, token);
            }
        } catch (err) {
            this.error = { message: err.response.data.failed, date: new Date() };
        }
    });

    sendSignUpRequest = flow(function*(email, password) {
        try {
            yield axios.post('/signup', { email, password });
            this.signInMode = true;
        } catch (err) {
            this.error = { message: err.response.data.failed, date: new Date() };
        }
    });

    sendFetchLoggedUserRequest = flow(function*() {
        try {
            const response = yield axios.get('/whoami');
            this.user = response.data;
        } catch (err) {
            this.error = { message: err.response.data.failed, date: new Date() };
        }
    });

    sendFetchUsersRequest = flow(function*() {
        try {
            const response = yield axios.get('/user-list');
            this.users = response.data;
        } catch (err) {
            this.error = { message: err.response.data.failed, date: new Date() };
        }
    });

    saveUser = (email, token) => {
        localStorage.setItem('user', email);
        localStorage.setItem('token', token);
    };

    resetUser = () => {
        this.user = null;
        localStorage.clear();
    };
}
export default new UserStore();
