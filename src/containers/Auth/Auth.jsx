import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { reaction } from 'mobx';
import { NotificationContainer, NotificationManager as alert } from 'react-notifications';

import AuthBox from '../../components/AuthBox/AuthBox';

@inject('UserStore')
@observer
class Auth extends Component {
    errorHandler = reaction(() => this.props.UserStore.error, error => alert.error(error.message));
    messageHandler = reaction(() => this.props.UserStore.message, message => alert.success(message));

    changeAuthMode = () => this.props.UserStore.changeAuthMode();
    fetchLoggedUser = () => this.props.UserStore.fetchLoggedUser();

    submitHandler = event => {
        event.preventDefault();
        const { UserStore } = this.props;
        const email = event.target.email.value;
        const password = event.target.password.value;
        if (UserStore.signInMode) {
            UserStore.signIn(email, password);
        } else {
            UserStore.signUp(email, password);
        }
    };

    render() {
        const { UserStore } = this.props;
        return (
            <div className="container">
                <NotificationContainer />
                <AuthBox
                    signInMode={UserStore.signInMode}
                    changeAuthMode={UserStore.changeAuthMode}
                    submitHandler={this.submitHandler}
                />
            </div>
        );
    }
}
export default Auth;
