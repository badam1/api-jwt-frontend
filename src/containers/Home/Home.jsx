import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import LoggedUser from '../../components/LoggedUser/LoggedUser';
import UserList from '../../components/UserList/UserList';

@inject('UserStore')
@withRouter
@observer
class Home extends Component {
    componentDidMount() {
        this.props.UserStore.fetchUsers();
        this.props.UserStore.fetchLoggedUser();
    }

    logout = () => this.props.UserStore.logout();

    render() {
        const { UserStore } = this.props;
        return (
            <div className="home">
                <LoggedUser user={UserStore.user} />
                <div className="u-margin-bottom-medium" />
                <button type="button" className="btn btn--blue" onClick={this.logout}>
                    Log out
                </button>
                <div className="u-margin-bottom-medium" />
                <UserList users={UserStore.users} />
            </div>
        );
    }
}
export default Home;
