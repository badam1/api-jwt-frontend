import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'mobx-react';
import axios from 'axios';
import 'react-notifications/lib/notifications.css';

import App from './App';
import './styles/index.scss';
import UserStore from './stores/UserStore/UserStore';
import registerServiceWorker from './registerServiceWorker';

axios.defaults.baseURL = 'https://accenture-react.bodansky.eu/user';
axios.interceptors.request.use(config => {
    const token = localStorage.getItem('token');
    config.headers['Authorization'] = token;
    return config;
});

ReactDOM.render(
    <Provider UserStore={UserStore}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);
registerServiceWorker();
