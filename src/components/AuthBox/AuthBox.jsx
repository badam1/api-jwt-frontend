import React from 'react';
import PropTypes from 'prop-types';
import AuthForm from '../AuthForm/AuthForm';

AuthBox.propTypes = {
    signInMode: PropTypes.bool.isRequired,
    changeAuthMode: PropTypes.func.isRequired,
    submitHandler: PropTypes.func.isRequired
};

function AuthBox({ signInMode, changeAuthMode, submitHandler }) {
    return (
        <div className="auth">
            <h2 className="auth__title">{signInMode ? 'Sign In' : 'Sign Up'}</h2>
            <AuthForm submitHandler={submitHandler} />
            <div className="u-margin-bottom-medium" />
            <button type="button" className="btn-inline" onClick={changeAuthMode}>
                {signInMode ? 'Sign up' : 'Sign in'}&nbsp;&rarr;
            </button>
        </div>
    );
}
export default AuthBox;
