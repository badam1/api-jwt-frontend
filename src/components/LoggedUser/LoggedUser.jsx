import React from 'react';
import PropTypes from 'prop-types';

LoggedUser.propTypes = {
    user: PropTypes.string.isRequired
};

function LoggedUser({ user }) {
    return (
        <div className="logged-user">
            <h1>Hello {user}</h1>
        </div>
    );
}
export default LoggedUser;
