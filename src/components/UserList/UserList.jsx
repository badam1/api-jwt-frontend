import React from 'react';
import PropTypes from 'prop-types';
import UserListItem from '../UserListItem/UserListItem';

UserList.propTypes = {
    users: PropTypes.arrayOf(PropTypes.string)
};

function UserList({ users }) {
    return (
        <div className="user-list">
            <h1 className="user-list__title">signed up users</h1>
            <ul>{users.map(user => <UserListItem key={user} user={user} />)}</ul>
        </div>
    );
}
export default UserList;
