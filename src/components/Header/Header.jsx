import React from 'react';

function Header() {
    return (
        <header className="app__header">
            <img src="favicon.ico" className="app__logo" alt="logo" />
            <h1 className="app__title">Welcome to React Course JWT authentication example</h1>
        </header>
    );
}
export default Header;
