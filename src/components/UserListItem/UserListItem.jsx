import React from 'react';
import PropTypes from 'prop-types';

UserListItem.propTypes = {
    user: PropTypes.string.isRequired
};

function UserListItem({ user }) {
    return <li className="user-list__item">{user}</li>;
}
export default UserListItem;
