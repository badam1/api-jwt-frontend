import React from 'react';
import PropTypes from 'prop-types';

AuthForm.propTypes = {
    submitHandler: PropTypes.func.isRequired
};

function AuthForm({ submitHandler }) {
    return (
        <form action="#" className="form" onSubmit={submitHandler}>
            <div className="u-margin-bottom-medium" />
            <div className="form__group">
                <input id="email" type="email" className="form__input" placeholder="Email" required />
                <label htmlFor="email" className="form__label">
                    Email
                </label>
            </div>
            <div className="form__group">
                <input id="password" type="password" className="form__input" placeholder="Password" required />
                <label htmlFor="password" className="form__label">
                    Password
                </label>
            </div>
            <div className="form__group">
                <button type="submit" className="btn btn--blue">
                    submit
                </button>
            </div>
        </form>
    );
}
export default AuthForm;
