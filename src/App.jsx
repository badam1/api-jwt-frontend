import React, { Component, Fragment } from 'react';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';

import Header from './components/Header/Header';
import Auth from './containers/Auth/Auth';
import asyncComponent from './hoc/asyncComponent/asyncComponent';

const asyncHome = asyncComponent(() => import('./containers/Home/Home'));

@inject('UserStore')
@withRouter
@observer
class App extends Component {
    componentDidMount() {
        this.props.UserStore.fetchLoggedUser();
    }

    render() {
        const { UserStore } = this.props;
        let routes = (
            <Fragment>
                <Route path="/auth" exact component={Auth} />
                <Redirect to="/auth" />
            </Fragment>
        );
        if (UserStore.user) {
            routes = (
                <Fragment>
                    <Route path="/" exact component={asyncHome} />
                    <Redirect to="/" />
                </Fragment>
            );
        }

        return (
            <div className="app">
                <Header />
                <Switch>{routes}</Switch>
            </div>
        );
    }
}

export default App;
